﻿using Cleanception.Application.Catalog.Products;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Application.Identity.Users;
using Cleanception.Domain.Catalog;
using Cleanception.Domain.Configuration;
using Cleanception.Infrastructure.Identity;
using Mapster;
using Org.BouncyCastle.Asn1.Cms;

namespace Cleanception.Infrastructure.Mapping;

public class MapsterSettings
{
    public static TypeAdapterConfig Configure()
    {
        // here we will define the type conversion / Custom-mapping
        // More details at https://github.com/MapsterMapper/Mapster/wiki/Custom-mapping

        var config = new TypeAdapterConfig();

        TypeAdapterConfig<SysAttribute, SysAttributeDto>.NewConfig()
             .Ignore(dest => dest.Variants);

        TypeAdapterConfig<Product, ProductDto>.NewConfig()
           .Map(dest => dest.Images, src => src.Images.Select(x => x.Image.ImageUrl).ToList());

        return config;
    }
}