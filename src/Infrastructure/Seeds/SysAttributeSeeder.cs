using System.Reflection;
using System.Text.Json;
using Cleanception.Application.Common.Interfaces;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Domain.Configuration;
using Cleanception.Infrastructure.Identity;
using Cleanception.Infrastructure.Persistence.Context;
using Cleanception.Infrastructure.Persistence.Initialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Cleanception.Infrastructure.Seeds;

public class SysAttributesSeeder : ICustomSeeder
{
    private readonly ISerializerService _serializerService;
    private readonly ApplicationDbContext _db;
    private readonly ILogger<SysAttributesSeeder> _logger;

    public SysAttributesSeeder(ISerializerService serializerService, ILogger<SysAttributesSeeder> logger, ApplicationDbContext db) {
        _serializerService = serializerService;
        _logger = logger;
        _db = db;
    }

    public async Task InitializeAsync(CancellationToken cancellationToken) {
        string? path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        _logger.LogInformation("Started To Seed SysAttributes.");

        string sysAttributesSource = await File.ReadAllTextAsync(path + "/Seeds/Resources/sysAttribute.json", cancellationToken);

        var fileSysAttributes = _serializerService.Deserialize<List<SysAttributeDto>>(sysAttributesSource);

        var sysAttributes = await _db.SysAttributes.AsNoTracking().ToListAsync();
        if (sysAttributes.Count == 0)
        {
            foreach (var item in fileSysAttributes)
             {
                var sysAttributeToAdd = new SysAttribute
                {
                    NameAr = item.NameAr,
                    NameEn = item.NameEn,
                    Variants = JsonSerializer.Serialize(item.Variants)
                };

                await _db.SysAttributes.AddAsync(sysAttributeToAdd);

                _logger.LogWarning($"Seed \"{item.NameEn}\" SysAttribute");
            }

            await _db.SaveChangesAsync();
        }

        _logger.LogInformation("Seeded SysAttributes.");
    }
}