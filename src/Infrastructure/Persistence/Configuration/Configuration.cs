﻿using Cleanception.Domain.Configuration;
using Cleanception.Infrastructure.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cleanception.Infrastructure.Persistence.Configuration;


public class ImageConfig : IEntityTypeConfiguration<Image>
{
    public void Configure(EntityTypeBuilder<Image> builder)
        => builder.ToTable("Images", SchemaNames.Configuration);
}

public class SysAttributeConfig : IEntityTypeConfiguration<SysAttribute>
{
    public void Configure(EntityTypeBuilder<SysAttribute> builder)
        => builder.ToTable("SysAttributes", SchemaNames.Configuration);
}

