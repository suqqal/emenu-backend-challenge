﻿using Cleanception.Domain.Catalog;
using DocumentFormat.OpenXml.VariantTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cleanception.Infrastructure.Persistence.Configuration;
public class ProductConfig : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
        => builder.ToTable("Products", SchemaNames.Catalog);
}

public class ProductAttributeConfig : IEntityTypeConfiguration<ProductAttribute>
{
    public void Configure(EntityTypeBuilder<ProductAttribute> builder) {
        builder.ToTable("ProductAttributes", SchemaNames.Catalog);
    }
}


public class ProductImageConfig : IEntityTypeConfiguration<ProductImage>
{
    public void Configure(EntityTypeBuilder<ProductImage> builder)
        => builder.ToTable("ProductImages", SchemaNames.Catalog);
}

