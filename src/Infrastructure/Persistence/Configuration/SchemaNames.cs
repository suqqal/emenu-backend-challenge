﻿namespace Cleanception.Infrastructure.Persistence.Configuration;

internal static class SchemaNames
{
    public static string Auditing = nameof(Auditing);
    public static string Identity = nameof(Identity);
    public static string Configuration = nameof(Configuration);
    public static string Catalog = nameof(Catalog);
}