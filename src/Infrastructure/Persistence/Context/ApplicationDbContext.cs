using Cleanception.Application.Common.Events;
using Cleanception.Application.Common.Interfaces;
using Cleanception.Domain.Catalog;
using Cleanception.Domain.Configuration;
using Cleanception.Domain.Notification;
using Cleanception.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Cleanception.Infrastructure.Persistence.Context;

public class ApplicationDbContext : BaseDbContext
{
    public ApplicationDbContext(DbContextOptions options, ICurrentUser currentUser, ISerializerService serializer, IOptions<DatabaseSettings> dbSettings, IEventPublisher events)
        : base(options, currentUser, serializer, dbSettings, events)
    {
    }

    #region Configuration
    public DbSet<Image> Images => Set<Image>();
    public DbSet<SysAttribute> SysAttributes => Set<SysAttribute>();
    #endregion

    #region Catalog
    public DbSet<Product> Products => Set<Product>();
    public DbSet<ProductAttribute> ProductAttributes => Set<ProductAttribute>();
    public DbSet<ProductImage> ProductImages => Set<ProductImage>();
    #endregion

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);
    }
}