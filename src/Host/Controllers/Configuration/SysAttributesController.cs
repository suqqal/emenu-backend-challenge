﻿using Cleanception.Application.Common.Models;
using Cleanception.Application.Configuration.SysAttributes;
using System.Text.Json;

namespace Cleanception.Host.Controllers.Configuration;

public class SysAttributesController : VersionedApiController
{
    [HttpPost("search")]
    [AllowAnonymous]
    [OpenApiOperation("Search for Sys Attributes using available filters.", "")]
    public async Task<PaginatedResult<SysAttributeSimplifyDto>> SearchAsync(SearchSysAttributesRequest request)
    {
        return await Mediator.Send(request);
    }

    [HttpGet("{id:guid}")]
    [OpenApiOperation("Get SysAttribute details.", "")]
    public async Task<Result<SysAttributeDto>> GetAsync(Guid id)
    {
        return await Mediator.Send(new GetSysAttributeRequest(id));
    }

}