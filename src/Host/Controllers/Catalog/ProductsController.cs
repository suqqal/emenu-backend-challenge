﻿using Cleanception.Application.Catalog.Products;
using Cleanception.Application.Common.Models;
using System.Text.Json;

namespace Cleanception.Host.Controllers.Configuration;

public class ProductsController : VersionedApiController
{
    [HttpPost]
    [OpenApiOperation("Create a new product.", "")]
    public async Task<Result<Guid>> CreateAsync([FromForm]CreateProductRequest request)
    {
        return await Mediator.Send(request);
    }

    [HttpPost("search")]
    [AllowAnonymous]
    [OpenApiOperation("Search for products using available filters.", "")]
    public async Task<PaginatedResult<ProductSimplifyDto>> SearchAsync(SearchProductsRequest request)
    {
        return await Mediator.Send(request);
    }

    [HttpGet("{id:guid}")]
    [OpenApiOperation("Get Product details.", "")]
    public async Task<Result<ProductDto>> GetAsync(Guid id)
    {
        return await Mediator.Send(new GetProductRequest(id));
    }

    [HttpPut]
    [OpenApiOperation("Update a Product.", "")]
    public async Task<Result<Guid>> UpdateAsync([FromForm]UpdateProductRequest request)
    {
        return await Mediator.Send(request);
    }

    [HttpDelete("{id:guid}")]
    [OpenApiOperation("Delete a Product.", "")]
    public async Task<Result<Guid>> DeleteAsync(Guid id)
    {
        return await Mediator.Send(new DeleteProductRequest(id));
    }


}