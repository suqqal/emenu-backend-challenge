﻿using Cleanception.Domain.Common.Contracts;
using Cleanception.Domain.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleanception.Domain.Catalog;
public class ProductAttribute : FullEntity, IAggregateRoot
{
    public Guid ProductId { get; set; }
    public Product Product { get; set; } = default!;
    public Guid SysAttributeId { get; set; }
    public SysAttribute SysAttribute { get; set; } = default!;
    public string Variants { get; set; } = default!;
}
