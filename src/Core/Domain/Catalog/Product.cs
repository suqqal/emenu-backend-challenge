﻿using Cleanception.Domain.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleanception.Domain.Catalog;
public class Product : FullEntity, IAggregateRoot
{
    public string NameEn { get; set; } = default!;
    public string NameAr { get; set; } = default!;
    public string? DescriptionEn { get; set; }
    public string? DescriptionAr { get; set; }
    public string? ImageUrl { get; set; }

    public ICollection<ProductImage> Images { get; set; }
    public ICollection<ProductAttribute> ProductAttributes { get; set; }

    public Product()
    {
        Images = new HashSet<ProductImage>();
        ProductAttributes = new HashSet<ProductAttribute>();
    }
}
