﻿using Cleanception.Domain.Attributes;
using Cleanception.Domain.Common.Contracts;
using Cleanception.Domain.Configuration;

namespace Cleanception.Domain.Catalog;

[SupportDeepSearch]
public class ProductImage : FullEntity, IAggregateRoot
{

    public DefaultIdType ProductId { get; set; }
    public Product Product { get; set; } = default!;
    public DefaultIdType ImageId { get; set; }
    public Image Image { get; set; } = default!;

}
