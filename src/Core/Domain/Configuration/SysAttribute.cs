﻿using Cleanception.Domain.Attributes;
using Cleanception.Domain.Common.Contracts;

namespace Cleanception.Domain.Configuration;

[SupportDeepSearch]
public class SysAttribute : FullEntity, IAggregateRoot
{
    [ColumnSupportDeepSearch]
    public string? NameEn { get; set; }
    [ColumnSupportDeepSearch]
    public string? NameAr { get; set; }
    public string Variants { get; set; } = default!;

}
