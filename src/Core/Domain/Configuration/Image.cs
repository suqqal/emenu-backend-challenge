﻿using Cleanception.Domain.Attributes;
using Cleanception.Domain.Common.Contracts;

namespace Cleanception.Domain.Configuration;

[SupportDeepSearch]
public class Image : FullEntity, IAggregateRoot
{
    [ColumnSupportDeepSearch]
    public string? Name { get; set; }

    public string? ImageUrl { get; set; }

}
