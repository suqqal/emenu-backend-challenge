using Cleanception.Application.Common.Interfaces;
using Cleanception.Domain.Common;
using Microsoft.AspNetCore.Http;

namespace Cleanception.Application.Common.FileStorage;

public interface IFileStorageService : ITransientService
{
    Task<string> UploadAsync<T>(IFormFile? file, FileType supportedFileType, CancellationToken cancellationToken = default);
    public void Remove(string? path);
}