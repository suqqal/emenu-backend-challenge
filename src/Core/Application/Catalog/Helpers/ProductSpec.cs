﻿using Cleanception.Application.Catalog.Products;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Specification;
using Cleanception.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleanception.Application.Catalog.Helpers;

public class ProductsBySearchRequestSpec : EntitiesByPaginationFilterSpec<Product, ProductSimplifyDto>
{
    public ProductsBySearchRequestSpec(SearchProductsRequest request)
        : base(request) =>
        Query
        .Where(x => x.IsActive == request.IsActive, request.IsActive.HasValue)
        .OrderBy(c => c.CreatedOn, !request.HasOrderBy());
}



public class ProductByIdSpec : Specification<Product, ProductDto>
{
    public ProductByIdSpec(Guid id) =>
        Query.Where(p => p.Id == id);
}