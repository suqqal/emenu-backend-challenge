﻿using Cleanception.Application.Common.Exceptions;
using Cleanception.Application.Common.FileStorage;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Application.Common.Specification;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Domain.Catalog;
using Cleanception.Domain.Common;
using Cleanception.Domain.Configuration;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using Image = Cleanception.Domain.Configuration.Image;

namespace Cleanception.Application.Catalog.Products;

public class CreateProductRequest : ICommand
{
    public string NameEn { get; set; } = default!;
    public string NameAr { get; set; } = default!;
    public string? DescriptionEn { get; set; }
    public string? DescriptionAr { get; set; }
    public bool? IsActive { get; set; }
    public IFormFile? MainImage { get; set; }
    public IFormFileCollection? Images { get; set; }
    public Guid? MainImageId { get; set; }
    public List<Guid>? ImagesIds { get; set; }
    public List<SysAttributeDto>? SysAttributes { get; set; }
}

public class CreateProductRequestValidator : AbstractValidator<CreateProductRequest>
{
    public CreateProductRequestValidator(IReadRepository<Product> repository, IReadRepository<SysAttribute> attRepository, IStringLocalizer<CreateProductRequestValidator> localizer) {

        RuleFor(p => p.NameAr)
           .NotEmpty()
           .MaximumLength(255)
               .MustAsync(async (name, ct) => !await repository.AnyAsync(new ExpressionSpecification<Product>(x => x.NameAr == name), ct))
                   .WithMessage((_, field) => localizer["Arabic name already exist.", field]);

        RuleFor(p => p.NameEn)
          .NotEmpty()
          .MaximumLength(255)
              .MustAsync(async (name, ct) => !await repository.AnyAsync(new ExpressionSpecification<Product>(x => x.NameEn == name), ct))
                  .WithMessage((_, field) => localizer["English name already exist.", field]);

        When(p => p.SysAttributes is not null, () =>
        {
            RuleForEach(p => p.SysAttributes)
                    .MustAsync(async (att, ct) => await attRepository.AnyAsync(new ExpressionSpecification<SysAttribute>(x => x.Id == att.Id), ct))
                        .WithMessage((_, field) => localizer["Attribute not found.", field]);
        });
    }
}

public class CreateProductRequestHandler : ICommandHandler<CreateProductRequest>
{

    private readonly IRepositoryWithEvents<Product> _repository;
    private readonly IRepositoryWithEvents<Image> _imageRepo;
    private readonly IStringLocalizer<CreateProductRequestHandler> _localizer;
    private readonly IFileStorageService _fileUpload;

    public CreateProductRequestHandler(IRepositoryWithEvents<Product> repository, IFileStorageService fileUpload, IRepositoryWithEvents<Image> imageRepo, IStringLocalizer<CreateProductRequestHandler> localizer) {
        _repository = repository;
        _fileUpload = fileUpload;
        _imageRepo = imageRepo;
        _localizer = localizer;
    }

    public async Task<Result<Guid>> Handle(CreateProductRequest request, CancellationToken cancellationToken)
    {
        var entity = new Product
        {
            NameAr = request.NameAr,
            NameEn = request.NameEn,
            DescriptionAr = request.DescriptionAr,
            DescriptionEn = request.DescriptionEn,
            IsActive = request.IsActive ?? false
        };

        if(request.SysAttributes is not null)
        {
            foreach(var att in request.SysAttributes)
            {
                entity.ProductAttributes.Add(new ProductAttribute
                {
                    SysAttributeId = att.Id,
                    Variants = JsonSerializer.Serialize(att.Variants)
                });
            }
        }

        if (request.MainImageId is not null)
        {
            var image = await _imageRepo.GetByIdAsync(request.MainImageId, cancellationToken);
            if (image is null)
                throw new NotFoundException(_localizer["Main image not found"]);
            entity.ImageUrl = image.ImageUrl;

            entity.Images.Add(new ProductImage
            {
                ImageId = image.Id
            });
        }
        else if(request.MainImage is not null)
        {
            entity.ImageUrl = await _fileUpload.UploadAsync<Product>(request.MainImage, FileType.Image, cancellationToken);
            var image = new Domain.Configuration.Image
            {
                ImageUrl = entity.ImageUrl,
                Name = request.MainImage.Name
            };

            await _imageRepo.AddAsync(image, cancellationToken);

            entity.Images.Add(new ProductImage
            {
                ImageId = image.Id
            });
        }

        if (request.ImagesIds is not null)
        {
            foreach (var imageId in request.ImagesIds)
            {
                var image = await _imageRepo.GetByIdAsync(imageId, cancellationToken);
                if (image is null)
                    throw new NotFoundException(_localizer["Image not found"]);
                entity.Images.Add(new ProductImage
                {
                    ImageId = image.Id
                });
            }
        }


        if (request.Images is not null)
        {
            foreach (var img in request.Images)
            {
                string imageUrl = await _fileUpload.UploadAsync<Product>(img, FileType.Image, cancellationToken);
                var image = new Domain.Configuration.Image
                {
                    ImageUrl = imageUrl,
                    Name = img.Name
                };

                await _imageRepo.AddAsync(image, cancellationToken);

                entity.Images.Add(new ProductImage
                {
                    ImageId = image.Id
                });
            }
        }

        await _repository.AddAsync(entity, cancellationToken);

        return await Result<Guid>.SuccessAsync(entity.Id);
    }
}