﻿using Cleanception.Application.Common.Exceptions;
using Cleanception.Application.Common.FileStorage;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Application.Common.Specification;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Domain.Catalog;
using Cleanception.Domain.Common;
using Cleanception.Domain.Configuration;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using Image = Cleanception.Domain.Configuration.Image;

namespace Cleanception.Application.Catalog.Products;

public class DeleteProductRequest : ICommand
{
    public Guid Id { get; set; }

    public DeleteProductRequest(DefaultIdType id) => Id = id;
}

public class DeleteProductRequestHandler : ICommandHandler<DeleteProductRequest>
{
    private readonly IRepositoryWithEvents<Product> _repository;
    private readonly IRepositoryWithEvents<ProductAttribute> _productAttributeRepo;
    private readonly IRepositoryWithEvents<Image> _imageRepo;
    private readonly IRepositoryWithEvents<ProductImage> _productImageRepo;
    private readonly IStringLocalizer<DeleteProductRequestHandler> _localizer;
    private readonly IFileStorageService _fileUpload;

    public DeleteProductRequestHandler(IRepositoryWithEvents<Product> repository, IFileStorageService fileUpload, IRepositoryWithEvents<Image> imageRepo, IStringLocalizer<DeleteProductRequestHandler> localizer, IRepositoryWithEvents<ProductAttribute> productAttributeRepo, IRepositoryWithEvents<ProductImage> productImageRepo) {
        _repository = repository;
        _fileUpload = fileUpload;
        _imageRepo = imageRepo;
        _localizer = localizer;
        _productAttributeRepo = productAttributeRepo;
        _productImageRepo = productImageRepo;
    }

    public async Task<Result<Guid>> Handle(DeleteProductRequest request, CancellationToken cancellationToken)
    {
        var product = await _repository.GetByIdAsync(request.Id, cancellationToken);
        if (product is null)
            throw new NotFoundException(_localizer["Product not found"]);

        var images = await _productImageRepo.ListAsync(new ExpressionSpecification<ProductImage>(x => x.ProductId == product.Id), cancellationToken);
        if (images is not null)
        {
            await _productImageRepo.DeleteRangeAsync(images, cancellationToken);
            foreach(var prodImage in images)
            {
                bool used = await _productImageRepo.AnyAsync(new ExpressionSpecification<ProductImage>(x => x.ProductId != product.Id && x.ImageId == prodImage.ImageId), cancellationToken);
                if (!used)
                {
                    var img = await _imageRepo.GetByIdAsync(prodImage.ImageId ,cancellationToken);
                    await _imageRepo.DeleteAsync(img, cancellationToken);
                }
            }
        }

        var productAttr = await _productAttributeRepo.ListAsync(new ExpressionSpecification<ProductAttribute>(x => x.ProductId == product.Id), cancellationToken);
        if (productAttr is not null)
            await _productAttributeRepo.DeleteRangeAsync(productAttr, cancellationToken);

        await _repository.DeleteAsync(product, cancellationToken);
        return await Result<Guid>.SuccessAsync(product.Id);
    }
}