﻿using Cleanception.Application.Catalog.Helpers;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Domain.Catalog;

namespace Cleanception.Application.Catalog.Products;

public class SearchProductsRequest : PaginationFilter, IQuery<PaginatedResult<ProductSimplifyDto>>
{
    public bool? IsActive { get; set; }
}

public class SearchProductsRequestHandler : IQueryHandler<SearchProductsRequest, PaginatedResult<ProductSimplifyDto>>
{
    private readonly IReadRepository<Product> _repository;

    public SearchProductsRequestHandler(IReadRepository<Product> repository) => _repository = repository;

    public async Task<PaginatedResult<ProductSimplifyDto>> Handle(SearchProductsRequest request, CancellationToken cancellationToken) {
        var spec = new ProductsBySearchRequestSpec(request);
        return await _repository.PaginatedListAsync(spec, request.PageNumber, request.PageSize, cancellationToken);
    }
}