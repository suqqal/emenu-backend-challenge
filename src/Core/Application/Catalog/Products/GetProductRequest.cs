﻿using Cleanception.Application.Catalog.Helpers;
using Cleanception.Application.Common.Exceptions;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Domain.Catalog;

namespace Cleanception.Application.Catalog.Products;

public class GetProductRequest : IQuery<Result<ProductDto>>
{
    public Guid Id { get; set; }

    public GetProductRequest(Guid id) => Id = id;
}


public class GetProductRequestHandler : IQueryHandler<GetProductRequest, Result<ProductDto>>
{
    private readonly IRepository<Product> _repository;
    private readonly IStringLocalizer<GetProductRequestHandler> _localizer;

    public GetProductRequestHandler(IRepository<Product> repository, IStringLocalizer<GetProductRequestHandler> localizer) => (_repository, _localizer) = (repository, localizer);

    public async Task<Result<ProductDto>> Handle(GetProductRequest request, CancellationToken cancellationToken) {
        var entity = await _repository.FirstOrDefaultAsync(new ProductByIdSpec(request.Id), cancellationToken);
        _ = entity ?? throw new NotFoundException(_localizer["Product Not Found.", request.Id]);
        return await Result<ProductDto>.SuccessAsync(entity);
    }
}