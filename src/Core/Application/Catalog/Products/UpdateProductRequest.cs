﻿using Cleanception.Application.Common.Exceptions;
using Cleanception.Application.Common.FileStorage;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Application.Common.Specification;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Domain.Catalog;
using Cleanception.Domain.Common;
using Cleanception.Domain.Configuration;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using Image = Cleanception.Domain.Configuration.Image;

namespace Cleanception.Application.Catalog.Products;

public class UpdateProductRequest : ICommand
{
    public Guid Id { get; set; }
    public string NameEn { get; set; } = default!;
    public string NameAr { get; set; } = default!;
    public string? DescriptionEn { get; set; }
    public string? DescriptionAr { get; set; }
    public bool? IsActive { get; set; }
    public IFormFile? MainImage { get; set; }
    public IFormFileCollection? Images { get; set; }
    public Guid? MainImageId { get; set; }
    public List<Guid>? ImagesIds { get; set; }
    public List<SysAttributeDto>? SysAttributes { get; set; }
}

public class UpdateProductRequestValidator : AbstractValidator<UpdateProductRequest>
{
    public UpdateProductRequestValidator(IReadRepository<Product> repository, IReadRepository<SysAttribute> attRepository, IStringLocalizer<UpdateProductRequestValidator> localizer) {

        RuleFor(p => p.NameAr)
           .NotEmpty()
           .MaximumLength(255)
               .MustAsync(async (req, name, ct) => !await repository.AnyAsync(new ExpressionSpecification<Product>(x => x.NameAr == name && x.Id != req.Id), ct))
                   .WithMessage((_, field) => localizer["Arabic name already exist.", field]);

        RuleFor(p => p.NameEn)
          .NotEmpty()
          .MaximumLength(255)
              .MustAsync(async (req, name, ct) => !await repository.AnyAsync(new ExpressionSpecification<Product>(x => x.NameEn == name && x.Id != req.Id), ct))
                  .WithMessage((_, field) => localizer["English name already exist.", field]);

        When(p => p.SysAttributes is not null, () =>
        {
            RuleForEach(p => p.SysAttributes)
                    .MustAsync(async (att, ct) => await attRepository.AnyAsync(new ExpressionSpecification<SysAttribute>(x => x.Id == att.Id), ct))
                        .WithMessage((_, field) => localizer["Attribute not found.", field]);
        });
    }
}

public class UpdateProductRequestHandler : ICommandHandler<UpdateProductRequest>
{

    private readonly IRepositoryWithEvents<Product> _repository;
    private readonly IRepositoryWithEvents<ProductAttribute> _productAttributeRepo;
    private readonly IRepositoryWithEvents<Image> _imageRepo;
    private readonly IStringLocalizer<UpdateProductRequestHandler> _localizer;
    private readonly IFileStorageService _fileUpload;

    public UpdateProductRequestHandler(IRepositoryWithEvents<Product> repository, IFileStorageService fileUpload, IRepositoryWithEvents<Image> imageRepo, IStringLocalizer<UpdateProductRequestHandler> localizer, IRepositoryWithEvents<ProductAttribute> productAttributeRepo) {
        _repository = repository;
        _fileUpload = fileUpload;
        _imageRepo = imageRepo;
        _localizer = localizer;
        _productAttributeRepo = productAttributeRepo;
    }

    public async Task<Result<Guid>> Handle(UpdateProductRequest request, CancellationToken cancellationToken)
    {
        var product = await _repository.GetByIdAsync(request.Id, cancellationToken);
        if (product is null)
            throw new NotFoundException(_localizer["Product not found"]);

        product.NameAr = request.NameAr;
        product.NameEn = request.NameEn;
        product.DescriptionAr = request.DescriptionAr;
        product.DescriptionEn = request.DescriptionEn;
        product.IsActive = request.IsActive!.Value;

        var oldAtts = await _productAttributeRepo.ListAsync(new ExpressionSpecification<ProductAttribute>(x => x.ProductId == product.Id), cancellationToken);

        if (oldAtts is not null)
            await _productAttributeRepo.DeleteRangeAsync(oldAtts, cancellationToken);

        if (request.SysAttributes is not null)
        {

            foreach(var att in request.SysAttributes)
            {
                product.ProductAttributes.Add(new ProductAttribute
                {
                    SysAttributeId = att.Id,
                    Variants = JsonSerializer.Serialize(att.Variants)
                });
            }
        }

        if (request.MainImageId is null)
        {
            product.ImageUrl = string.Empty;
        }
        else if (request.MainImage is not null)
        {
            var image = await _imageRepo.GetByIdAsync(request.MainImageId, cancellationToken);
            if (image is null)
                throw new NotFoundException(_localizer["Main image not found"]);
            product.ImageUrl = image.ImageUrl;

            product.Images.Add(new ProductImage
            {
                ImageId = image.Id
            });
        }
        else if(request.MainImage is not null)
        {
            product.ImageUrl = await _fileUpload.UploadAsync<Product>(request.MainImage, FileType.Image, cancellationToken);
            var image = new Domain.Configuration.Image
            {
                ImageUrl = product.ImageUrl,
                Name = request.MainImage.Name
            };

            await _imageRepo.AddAsync(image, cancellationToken);

            product.Images.Add(new ProductImage
            {
                ImageId = image.Id
            });
        }

        if (request.ImagesIds is not null)
        {
            foreach (var imageId in request.ImagesIds)
            {
                var image = await _imageRepo.GetByIdAsync(imageId, cancellationToken);
                if (image is null)
                    throw new NotFoundException(_localizer["Image not found"]);
                product.Images.Add(new ProductImage
                {
                    ImageId = image.Id
                });
            }
        }

        if (request.Images is not null)
        {
            foreach (var img in request.Images)
            {
                string imageUrl = await _fileUpload.UploadAsync<Product>(img, FileType.Image, cancellationToken);
                var image = new Domain.Configuration.Image
                {
                    ImageUrl = imageUrl,
                    Name = img.Name
                };

                await _imageRepo.AddAsync(image, cancellationToken);

                product.Images.Add(new ProductImage
                {
                    ImageId = image.Id
                });
            }
        }

        await _repository.UpdateAsync(product, cancellationToken);

        return await Result<Guid>.SuccessAsync(product.Id);
    }
}