﻿using Cleanception.Application.Common.Interfaces;
using Cleanception.Application.Common.Models;
using Cleanception.Domain.Catalog;

namespace Cleanception.Application.Catalog.Products;

public class ProductSimplifyDto : IDto
{
    public Guid Id { get; set; }
    public string? NameEn { get; set; }
    public string? NameAr { get; set; }
    public string? DescriptionEn { get; set; }
    public string? DescriptionAr { get; set; }
    public string? ImageUrl { get; set; }
}

public class ProductRawDto : ProductSimplifyDto
{

}

public class ProductDto : ProductRawDto
{
    public List<string>? Images { get; set; }
    public List<ProductAttribute>? ProductAttributes { get; set; }
}

public class ProductDetailsDto : BaseDetailsDto
{
    public Guid Id { get; set; }
    public string? NameEn { get; set; }
    public string? NameAr { get; set; }
    public string? DescriptionEn { get; set; }
    public string? DescriptionAr { get; set; }
    public string? ImageUrl { get; set; }
    public List<string>? Images { get; set; }
    public List<ProductAttribute>? ProductAttributes { get; set; }

}