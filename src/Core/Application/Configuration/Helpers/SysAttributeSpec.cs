﻿using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Specification;
using Cleanception.Application.Configuration.SysAttributes;
using Cleanception.Domain.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleanception.Application.Configuration.Helpers;

public class SysAttributeByIdSpec : Specification<SysAttribute, SysAttributeDto>
{
    public SysAttributeByIdSpec(Guid id) =>
        Query.Where(p => p.Id == id);
}

public class SysAttributesBySearchRequestSpec : EntitiesByPaginationFilterSpec<SysAttribute, SysAttributeSimplifyDto>
{
    public SysAttributesBySearchRequestSpec(SearchSysAttributesRequest request)
        : base(request) =>
        Query.Where(x => x.IsActive == request.IsActive, request.IsActive.HasValue)
             .OrderBy(c => c.CreatedOn, !request.HasOrderBy());
}