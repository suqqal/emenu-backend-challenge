using Cleanception.Application.Common.Interfaces;
using Cleanception.Application.Common.Models;
using System.Text.Json.Serialization;

namespace Cleanception.Application.Configuration.SysAttributes;

public class SysAttributeSimplifyDto : IDto
{
    public Guid Id { get; set; }
    public string? NameEn { get; set; }
    public string? NameAr { get; set; }
}

public class SysAttributeRawDto : SysAttributeSimplifyDto
{
}

public class SysAttributeDto : SysAttributeRawDto
{
    public List<string>? Variants { get; set; }
}

public class SysAttributeDetailsDto : BaseDetailsDto
{
    public Guid Id { get; set; }
    public string? NameEn { get; set; }
    public string? NameAr { get; set; }
    public List<string>? Variants { get; set; }
}
