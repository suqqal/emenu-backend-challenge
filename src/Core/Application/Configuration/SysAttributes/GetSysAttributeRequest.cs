﻿using Cleanception.Application.Common.Exceptions;
using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Application.Configuration.Helpers;
using Cleanception.Application.Configuration.SysAttributes;
using System.Text.Json;

namespace Cleanception.Application.Configuration.SysAttributes;

public class GetSysAttributeRequest : IQuery<Result<SysAttributeDto>>
{
    public Guid Id { get; set; }

    public GetSysAttributeRequest(Guid id) => Id = id;
}


public class GetCityRequestHandler : IQueryHandler<GetSysAttributeRequest, Result<SysAttributeDto>>
{
    private readonly IRepository<Domain.Configuration.SysAttribute> _repository;
    private readonly IStringLocalizer<GetCityRequestHandler> _localizer;

    public GetCityRequestHandler(IRepository<Domain.Configuration.SysAttribute> repository, IStringLocalizer<GetCityRequestHandler> localizer) => (_repository, _localizer) = (repository, localizer);

    public async Task<Result<SysAttributeDto>> Handle(GetSysAttributeRequest request, CancellationToken cancellationToken) {
        var entity = await _repository.GetByIdAsync(request.Id, cancellationToken);

        _ = entity ?? throw new NotFoundException(_localizer["Sys Attribute Not Found.", request.Id]);
        var x = JsonSerializer.Deserialize<List<string>>(entity.Variants);
        return await Result<SysAttributeDto>.SuccessAsync(new SysAttributeDto
        {
            Id = entity.Id,
            NameAr = entity.NameAr,
            NameEn = entity.NameEn,
            Variants = x
        });
    }
}