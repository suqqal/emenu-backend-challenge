using Cleanception.Application.Common.Messaging;
using Cleanception.Application.Common.Models;
using Cleanception.Application.Common.Persistence;
using Cleanception.Application.Configuration.Helpers;

namespace Cleanception.Application.Configuration.SysAttributes;

public class SearchSysAttributesRequest : PaginationFilter, IQuery<PaginatedResult<SysAttributeSimplifyDto>>
{
    public bool? IsActive { get; set; }
}

public class SearchSysAttributesRequestHandler : IQueryHandler<SearchSysAttributesRequest, PaginatedResult<SysAttributeSimplifyDto>>
{
    private readonly IReadRepository<Domain.Configuration.SysAttribute> _repository;

    public SearchSysAttributesRequestHandler(IReadRepository<Domain.Configuration.SysAttribute> repository) => _repository = repository;

    public async Task<PaginatedResult<SysAttributeSimplifyDto>> Handle(SearchSysAttributesRequest request, CancellationToken cancellationToken)
    {
        var spec = new SysAttributesBySearchRequestSpec(request);
        return await _repository.PaginatedListAsync(spec, request.PageNumber, request.PageSize, cancellationToken);
    }
}